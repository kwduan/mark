﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;
using MARK_Alpha.Kit.Interfaces;

namespace MARK_Alpha.Kit.Impl
{
    class AbsolutePositionRecorder:InfoRecorderInterface
    {
        #region Fields
        private Skeleton skel;
        private Dictionary<String, Double[]> previousRecord = new Dictionary<string, double[]>();
        private Dictionary<JointType, double[]> dPositions = new Dictionary<JointType, double[]>();
        private String imageIndex;
        private JointType[] jointTypes;
        private DateTime time;
        private StreamWriter appendWriter = null;
        #endregion


        #region Public Methods
        public AbsolutePositionRecorder()
        {

        }

        void InfoRecorderInterface.SetSkeleton(Skeleton Skel, string ImageIndex)
        {
            this.skel = Skel;
            this.imageIndex = ImageIndex;
        }

        void InfoRecorderInterface.ResetPreviousRecord()
        {
            this.previousRecord = new Dictionary<string, double[]>();
        }

        void InfoRecorderInterface.Execute()
        {
            foreach (JointType jt in this.jointTypes)
            {
                if (this.dPositions.ContainsKey(jt))
                {
                    this.dPositions.Remove(jt);
                    this.dPositions.Add(jt, this.positionExtractor(this.skel, jt));
                }
                else
                {
                    this.dPositions.Add(jt, this.positionExtractor(this.skel, jt));
                }
            }           
            this.positionFileWriter(this.time);
            
        }

        void InfoRecorderInterface.WriteVoidLine()
        {
            this.positionFileWriter(this.time);
        }

        void InfoRecorderInterface.SetJointTyps(JointType[] Jts)
        {
            this.jointTypes = Jts;
        }

        void InfoRecorderInterface.SetRecordTime(DateTime Time)
        {
            this.time = Time;
        }


        void InfoRecorderInterface.SetStreamWriter(StreamWriter appendSW)
        {
            this.appendWriter = appendSW;
        }
        #endregion

        #region Private Methods

        private double[] positionExtractor(Skeleton Skel, JointType jt)
        {
            Joint j = Skel.Joints[jt];
            double posX = j.Position.X;
            double posY = j.Position.Y;
            double posZ = j.Position.Z;
            double[] position = new double[] { posX, posY, posZ };
            return position;

        }

        private void positionFileWriter(DateTime time)
        {
            String tempWriteLine = "";
            if (this.dPositions.Count != 0)
            {
                foreach (KeyValuePair<JointType, Double[]> positionEntry in this.dPositions)
                {
                    tempWriteLine += positionEntry.Key + "\t" + positionEntry.Value[0] + "\t" + positionEntry.Value[1] + "\t" + positionEntry.Value[2] + "\t";
                }
            }
            else
            {
                tempWriteLine += "null" + "\t";
            }
            string format = "yyyy-MM-dd_HH:mm:ss.fff";
            String sTime = time.ToString(format);

            try
            {
                if (this.appendWriter != null){
                    this.appendWriter.WriteLine(sTime + "\t" + tempWriteLine );
                }
                else{
                    String tempPath = "";
                    string folderpath = Properties.Resources.DistanceRecordFolderPath;
                    tempPath = folderpath + this.imageIndex + "_Position" + ".txt";

                    if (File.Exists(tempPath))
                    //File path is stored in Resources.resx
                    {
                        using (StreamWriter sw = File.AppendText(tempPath))
                        {
                            sw.WriteLine(tempWriteLine + "\t" + sTime);
                        }
                    }
                    else
                    {
                        using (StreamWriter sw = File.CreateText(tempPath))
                        {
                            sw.WriteLine(tempWriteLine + "\t" + sTime);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (e.Source != null)
                {
                    Console.WriteLine(e.Message);
                    throw;
                }
            }
        } 
        #endregion
    }
}

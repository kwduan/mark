﻿namespace MARK_Alpha.Kit.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;
    using Microsoft.Kinect;
    using MARK_Alpha.Kit.Interfaces;

    /// <summary>
    /// Extract each discrete distance in trajectory and write it to text files
    /// Author: Kewei Duan
    /// </summary>
    class DistanceRecorder : InfoRecorderInterface
    {
        # region Fields
        private Skeleton skel;
        private Dictionary<String, Double[]> previousRecord = new Dictionary<string, double[]>();
        private string imageIndex;
        private Dictionary<JointType, double> dDistances = new Dictionary<JointType,double>();
        DateTime time;
        private JointType[] jointTypes;
        private StreamWriter appendWriter = null;
        #endregion

        #region Public Methods
        public DistanceRecorder()
        {

        }

        public DistanceRecorder(Skeleton Skel)
            : this()
        {
            this.skel = Skel;
        }

        void InfoRecorderInterface.ResetPreviousRecord()
        {
            this.previousRecord = new Dictionary<string, double[]>();
        }
        void InfoRecorderInterface.SetSkeleton(Skeleton Skel, string ImageIndex)
        {
            this.skel = Skel;
            this.imageIndex = ImageIndex;
        }
        void InfoRecorderInterface.Execute()
        { 
            foreach (JointType jt in this.jointTypes)
            {
                if (this.dDistances.ContainsKey(jt))
                {
                    this.dDistances.Remove(jt);
                    this.dDistances.Add(jt, this.DistanceExtractor(this.skel, jt));
                }
                else
                {
                    this.dDistances.Add(jt, this.DistanceExtractor(this.skel, jt));
                }
            }      
            this.DistanceFileWriter(this.time);            
        }

        void InfoRecorderInterface.WriteVoidLine()
        {
            this.DistanceFileWriter(this.time);
        }

        void InfoRecorderInterface.SetJointTyps(JointType[] Jts)
        {
            this.jointTypes = Jts;
        }

        void InfoRecorderInterface.SetRecordTime(DateTime Time)
        {
            this.time = Time;
        }

        void InfoRecorderInterface.SetStreamWriter(StreamWriter appendSW)
        {
            this.appendWriter = appendSW;
        }
        #endregion

        #region Private Methods
        private double DistanceExtractor(Skeleton Skel, JointType jt)
        {
            double distance = 0.0;
            Joint j = Skel.Joints[jt];
            string key = jt.ToString();
            double posX = j.Position.X;
            double posY = j.Position.Y;
            double posZ = j.Position.Z;
            if (this.previousRecord != null && this.previousRecord.ContainsKey(key))
            {
                double[] tempArray = this.previousRecord[key];
                distance = Math.Sqrt(Math.Pow((posX - tempArray[0]), 2) + Math.Pow((posY - tempArray[1]), 2) + Math.Pow((posZ - tempArray[2]), 2));
            }


            double[] previousHeadRecord = new double[3] { posX, posY, posZ };
            if (this.previousRecord.ContainsKey(key))
            {
                this.previousRecord[key] = previousHeadRecord;
            }
            else
            {
                this.previousRecord.Add(key, previousHeadRecord);
            }

            return distance;
        }

        private void DistanceFileWriter(DateTime time)
        {
            String tempWriteLine = "";
            if (this.dDistances.Count != 0)
            {
                foreach (KeyValuePair<JointType, Double> distanceEntry in this.dDistances)
                {
                    tempWriteLine += distanceEntry.Key + "\t" + distanceEntry.Value + "\t"; ;
                }
            }
            else
            {
                tempWriteLine += "null" + "\t";
            }
            string format = "yyyy-MM-dd_HH:mm:ss.fff";
            String sTime = time.ToString(format);

            try
            {
                if (this.appendWriter != null)
                {
                    this.appendWriter.WriteLine(sTime + "\t" + tempWriteLine);
                }
                else
                {
                    String tempPath = "";
                    string folderpath = Properties.Resources.DistanceRecordFolderPath;
                    tempPath = folderpath + this.imageIndex + "_Distance" + ".txt";

                    if (File.Exists(tempPath))
                    //File path is stored in Resources.resx
                    {
                        using (StreamWriter sw = File.AppendText(tempPath))
                        {
                            sw.WriteLine(sTime + "\t" +tempWriteLine);
                        }
                    }
                    else
                    {
                        using (StreamWriter sw = File.CreateText(tempPath))
                        {
                            sw.WriteLine(sTime + "\t" + tempWriteLine);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (e.Source != null)
                {
                    Console.WriteLine(e.Message);
                    throw;
                }
            }
        }
        #endregion
    }
}

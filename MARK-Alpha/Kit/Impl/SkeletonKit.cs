﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Microsoft.Kinect;
using MARK_Alpha.Kit.Interfaces;

namespace MARK_Alpha.Kit.Impl
{
    /// <summary>
    /// Draw control Image from skeleton stream, 
    /// as well as a manager of all ther information Recorder
    /// Author: Kewei Duan
    /// </summary>
    class SkeletonKit
    {
        # region Constants
        /// <summary>
        /// Width of output drawing
        /// </summary>
        private const float RenderWidth = 640.0f;

        /// <summary>
        /// Height of our output drawing
        /// </summary>
        private const float RenderHeight = 480.0f;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Thickness of body center ellipse
        /// </summary>
        private const double BodyCenterThickness = 10;

        # endregion

        #region Fields
        /// <summary>
        /// Thickness of clip edge rectangles
        /// </summary>
        private const double ClipBoundsThickness = 10;

        /// <summary>
        /// Brush used to draw skeleton center point
        /// </summary>
        private readonly Brush centerPointBrush = Brushes.Blue;

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Pen used for drawing bones that are currently tracked
        /// </summary>
        private readonly Pen trackedBonePen = new Pen(Brushes.Green, 6);

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// 
        /// </summary>
        private KinectSensor sensor;

        /// <summary>
        /// Drawing group for skeleton rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// 
        /// </summary>
        private List<InfoRecorderInterface> lRecorder;

        /// <summary>
        /// 
        /// </summary>
        private string imageIndex;

        /// <summary>
        /// 
        /// </summary>
        private bool isRecord = false;

        /// <summary>
        /// 
        /// </summary>
        private JointType[] ajt;

        private Dictionary<string, StreamWriter> dAppendSW = new Dictionary<string,StreamWriter>();
        # endregion

        # region Public Methods

        public SkeletonKit(KinectSensor Sensor, DrawingGroup DG, DrawingImage DI, List<InfoRecorderInterface> LRecorder)
        {
            this.sensor = Sensor;
            this.drawingGroup = DG;
            this.imageSource = DI;
            this.lRecorder = LRecorder;
            this.ajt = new JointType[] {
                JointType.Head,
                JointType.ShoulderCenter,
                JointType.ShoulderLeft,
                JointType.ShoulderRight,
                JointType.WristLeft,
                JointType.WristRight,
                JointType.ElbowLeft,
                JointType.ElbowRight,
                JointType.HandLeft,
                JointType.HandRight,
            };
        }

        public void InitialStreamWriters()
        {
            foreach (InfoRecorderInterface iri in this.lRecorder)
            {
                string recorderName = "";
                if (iri.GetType() == typeof(AbsolutePositionRecorder))
                {
                    recorderName = "position";
                }
                else if (iri.GetType() == typeof(DistanceRecorder))
                {
                    recorderName = "distance";
                }
                String tempPath = "";
                string folderpath = Properties.Resources.DistanceRecordFolderPath;
                tempPath = folderpath + this.imageIndex + "_" + recorderName + ".txt";

                if (!File.Exists(tempPath))
                {
                    using (File.Create(tempPath))
                    {
                        //Here is just to make sure the Streamwriter returned by File.Create will dispose itself.
                    }
                }
                
                this.dAppendSW.Add(recorderName, File.AppendText(tempPath));
            }
        }

        public void ResetDistanceRecorder(){
            foreach (InfoRecorderInterface iri in this.lRecorder)
            {
                iri.ResetPreviousRecord();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="ImageIndex"></param>
        public void SetImageIndex(string ImageIndex)
        {
            this.imageIndex = ImageIndex;
        }

        public void SetIsRecord(bool IsRecord)
        {
            this.isRecord = IsRecord;
        }

        public void DisposeAllStreamWriters()
        {
            foreach (KeyValuePair<String, StreamWriter> sWEntry in this.dAppendSW)
            {
                sWEntry.Value.Dispose();
            }
        }

        public void DisposeStreamWriter()
        {

        }

        /// <summary>
        /// Event handler for Kinect sensor's SkeletonFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        public void SensorSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            DateTime time = DateTime.Now;

            Skeleton[] skeletons = new Skeleton[0];

            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    skeletonFrame.CopySkeletonDataTo(skeletons);
                }
            }

            using (DrawingContext dc = this.drawingGroup.Open())
            {
                // Draw a transparent background to set the render size
                dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, RenderWidth, RenderHeight));

                if (skeletons.Length != 0)
                {
                    foreach (Skeleton skel in skeletons)
                    {
                        RenderClippedEdges(skel, dc);

                        if (skel.TrackingState == SkeletonTrackingState.Tracked)
                        {
                            this.DrawBonesAndJoints(skel, dc);
                            if (this.isRecord)
                            {
                                foreach (InfoRecorderInterface iri in this.lRecorder)
                                {
                                    string recorderName = "";
                                    if (iri.GetType() == typeof(AbsolutePositionRecorder))
                                    {
                                        recorderName = "position";
                                    }
                                    else if (iri.GetType() == typeof(DistanceRecorder))
                                    {
                                        recorderName = "distance";
                                    }

                                    iri.SetStreamWriter(this.dAppendSW[recorderName]);
                                    iri.SetRecordTime(time);
                                    iri.SetJointTyps(this.ajt);
                                    iri.SetSkeleton(skel, this.imageIndex);
                                    iri.Execute();
                                }
                            }
                        }
                        else if (skel.TrackingState == SkeletonTrackingState.PositionOnly)
                        {
                            dc.DrawEllipse(
                            this.centerPointBrush,
                            null,
                            this.SkeletonPointToScreen(skel.Position),
                            BodyCenterThickness,
                            BodyCenterThickness);
                            if (this.isRecord)
                            {
                                foreach (InfoRecorderInterface iri in this.lRecorder)
                                {
                                    string recorderName = "";
                                    if (iri.GetType() == typeof(AbsolutePositionRecorder))
                                    {
                                        recorderName = "position";
                                    }
                                    else if (iri.GetType() == typeof(DistanceRecorder))
                                    {
                                        recorderName = "distance";
                                    }

                                    iri.SetStreamWriter(this.dAppendSW[recorderName]);
                                    iri.SetRecordTime(time);
                                    iri.WriteVoidLine();
                                }
                            }
                        }
                        else if (skel.TrackingState == SkeletonTrackingState.NotTracked)
                        {
                            if (this.isRecord)
                            {
                                foreach (InfoRecorderInterface iri in this.lRecorder)
                                {
                                    string recorderName = "";
                                    if (iri.GetType() == typeof(AbsolutePositionRecorder))
                                    {
                                        recorderName = "position";
                                    }
                                    else if (iri.GetType() == typeof(DistanceRecorder))
                                    {
                                        recorderName = "distance";
                                    }

                                    iri.SetStreamWriter(this.dAppendSW[recorderName]);
                                    iri.SetRecordTime(time);
                                    iri.WriteVoidLine();
                                }
                            }
                        }
                    }
                }

                // prevent drawing outside of our render area
                this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, RenderWidth, RenderHeight));
            }
        }

        # endregion

        # region Private Methods

        /// <summary>
        /// Draws indicators to show which edges are clipping skeleton data
        /// </summary>
        /// <param name="skeleton">skeleton to draw clipping information for</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private static void RenderClippedEdges(Skeleton skeleton, DrawingContext drawingContext)
        {
            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Bottom))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, RenderHeight - ClipBoundsThickness, RenderWidth, ClipBoundsThickness));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Top))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, RenderWidth, ClipBoundsThickness));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Left))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, ClipBoundsThickness, RenderHeight));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Right))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(RenderWidth - ClipBoundsThickness, 0, ClipBoundsThickness, RenderHeight));
            }
        }
       
        /// <summary>
        /// Draws a skeleton's bones and joints
        /// </summary>
        /// <param name="skeleton">skeleton to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBonesAndJoints(Skeleton skeleton, DrawingContext drawingContext)
        {
            // Render Torso
            this.DrawBone(skeleton, drawingContext, JointType.Head, JointType.ShoulderCenter);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderLeft);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderRight);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.Spine);
            this.DrawBone(skeleton, drawingContext, JointType.Spine, JointType.HipCenter);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipLeft);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipRight);

            // Left Arm
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderLeft, JointType.ElbowLeft);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowLeft, JointType.WristLeft);
            this.DrawBone(skeleton, drawingContext, JointType.WristLeft, JointType.HandLeft);

            // Right Arm
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderRight, JointType.ElbowRight);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowRight, JointType.WristRight);
            this.DrawBone(skeleton, drawingContext, JointType.WristRight, JointType.HandRight);

            // Left Leg
            this.DrawBone(skeleton, drawingContext, JointType.HipLeft, JointType.KneeLeft);
            this.DrawBone(skeleton, drawingContext, JointType.KneeLeft, JointType.AnkleLeft);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleLeft, JointType.FootLeft);

            // Right Leg
            this.DrawBone(skeleton, drawingContext, JointType.HipRight, JointType.KneeRight);
            this.DrawBone(skeleton, drawingContext, JointType.KneeRight, JointType.AnkleRight);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleRight, JointType.FootRight);

            // Render Joints
            foreach (Joint joint in skeleton.Joints)
            {
                Brush drawBrush = null;

                if (joint.TrackingState == JointTrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (joint.TrackingState == JointTrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, this.SkeletonPointToScreen(joint.Position), JointThickness, JointThickness);
                }
            }
        }

        /// <summary>
        /// Maps a SkeletonPoint to lie within our render space and converts to Point
        /// </summary>
        /// <param name="skelpoint">point to map</param>
        /// <returns>mapped point</returns>
        private Point SkeletonPointToScreen(SkeletonPoint skelpoint)
        {
            // Convert point to depth space.  
            // We are not using depth directly, but we do want the points in our 640x480 output resolution.
            DepthImagePoint depthPoint = this.sensor.CoordinateMapper.MapSkeletonPointToDepthPoint(skelpoint, DepthImageFormat.Resolution640x480Fps30);
            return new Point(depthPoint.X, depthPoint.Y);
        }

        /// <summary>
        /// Draws a bone line between two joints
        /// </summary>
        /// <param name="skeleton">skeleton to draw bones from</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="jointType0">joint to start drawing from</param>
        /// <param name="jointType1">joint to end drawing at</param>
        private void DrawBone(Skeleton skeleton, DrawingContext drawingContext, JointType jointType0, JointType jointType1)
        {
            Joint joint0 = skeleton.Joints[jointType0];
            Joint joint1 = skeleton.Joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == JointTrackingState.NotTracked ||
                joint1.TrackingState == JointTrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == JointTrackingState.Inferred &&
                joint1.TrackingState == JointTrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if (joint0.TrackingState == JointTrackingState.Tracked && joint1.TrackingState == JointTrackingState.Tracked)
            {
                drawPen = this.trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, this.SkeletonPointToScreen(joint0.Position), this.SkeletonPointToScreen(joint1.Position));
        }
        # endregion
    }
}

﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using MARK_Alpha.Kit.Impl;
using MARK_Alpha.Kit.Interfaces;

namespace MARK_Alpha
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// Author: Kewei Duan
    /// </summary>
    public partial class MainWindow : Window
    {
        # region Constants

        /// <summary>
        /// Maximum number of sensors to support for reconstruction
        /// </summary>
        private const int MaxSensors = 2;

        # endregion

        # region Fields

        /// <summary>
        /// Kinect sensors chooser objects
        /// </summary>
        private List<KinectSensorChooser> sensorChoosers = new List<KinectSensorChooser>();

        /// <summary>
        /// Active Kinect sensors
        /// </summary>
        private List<KinectSensor> sensors = new List<KinectSensor>();

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<String, SkeletonKit> dSkeletonKit = new Dictionary<string, SkeletonKit>();

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<String, Image> dImage = new Dictionary<string, Image>();

        /// <summary>
        /// 
        /// </summary>
        private Queue<Image> qImage = new Queue<Image>();

        # endregion

        #region Public Methods
        
        public MainWindow()
        {
            InitializeComponent();
        }
        
        #endregion

        #region Private Methods

        /// <summary>
        /// Execute startup tasks
        /// </summary>
        /// <param name="sender">Object sending the event</param>
        /// <param name="e">Event arguments</param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            this.qImage.Enqueue(this.Image1);
            this.qImage.Enqueue(this.Image2);
            this.AddSensorChooser(new Thickness(10, 0, 0, 5), false);

            if (null == this.sensors || (null != this.sensors && 0 == this.sensors.Count))
            {
                this.showStatusMessage(Properties.Resources.NoReadyKinect);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (0 != this.sensors.Count)
            {
                foreach (KinectSensor sensor in this.sensors)
                {
                    sensor.Stop();
                }
            }
            foreach (KeyValuePair<String, SkeletonKit> sKEntry in this.dSkeletonKit)
            {
                sKEntry.Value.DisposeAllStreamWriters();
            }
            
        }

        /// <summary>
        /// Add Sensor Chooser objects to UI
        /// </summary>
        /// <param name="margin">The margin to set around the sensor chooser icon in the UI.</param>
        /// <param name="hide"> Whether to create the sensor chooser with a hidden icon.</param>
        private void AddSensorChooser(Thickness margin, bool hide)
        {
            this.sensorChoosers.Add(new KinectSensorChooser());
            KinectSensorChooser sensorChooser = this.sensorChoosers[this.sensorChoosers.Count - 1];

            this.sensorchooserStackPanel.Children.Add(new KinectSensorChooserUI());
            KinectSensorChooserUI ui =
                this.sensorchooserStackPanel.Children[this.sensorchooserStackPanel.Children.Count - 1] as KinectSensorChooserUI;
            if (ui != null)
            {
                ui.Margin = margin;
            }

            ui.KinectSensorChooser = sensorChooser;
            sensorChooser.KinectChanged += this.OnKinectSensorChanged;
            sensorChooser.Start();

            if (hide)
            {
                ui.Visibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// Handler function for Kinect changed event
        /// </summary>
        /// <param name="sender">Event generator</param>
        /// <param name="e">Event parameter</param>
        private void OnKinectSensorChanged(object sender, KinectChangedEventArgs e)
        {
            // Check new Sensor's status
            if (null != e.NewSensor)
            {
                KinectSensor result = null;
                result = this.sensors.Find(c => (c.DeviceConnectionId == e.NewSensor.DeviceConnectionId));

                // new sensor
                if (null == result && KinectStatus.Connected == e.NewSensor.Status)
                {
                    // Make sensor chooser visible
                    KinectSensorChooserUI ui =
                        this.sensorchooserStackPanel.Children[this.sensorchooserStackPanel.Children.Count - 1] as KinectSensorChooserUI;
                    if (ui != null)
                    {
                        ui.Visibility = Visibility.Visible;
                    }

                    this.sensors.Add(e.NewSensor);
                    this.showStatusMessage(this.sensors.Count+" sensor(s) found");

                    DrawingGroup tempDG = new DrawingGroup();

                    DrawingImage tempDI = new DrawingImage(tempDG);

                    List<InfoRecorderInterface> liri = new List<InfoRecorderInterface>();
                    liri.Add(new DistanceRecorder());
                    liri.Add(new AbsolutePositionRecorder());

                    SkeletonKit sK = new SkeletonKit(e.NewSensor, tempDG, tempDI, liri);
                    this.dSkeletonKit.Add(e.NewSensor.DeviceConnectionId, sK);

                    bool tempSensorSuccessIndicator = true;

                    if (this.qImage.Count != 0)
                    {
                        Image tempImage = qImage.Dequeue();
                        tempImage.Source = tempDI;
                        this.dImage.Add(e.NewSensor.DeviceConnectionId, tempImage);
                        sK.SetImageIndex(tempImage.Name);
                        sK.InitialStreamWriters();
                    }
                    else
                    {
                        tempSensorSuccessIndicator = false;
                        this.showStatusMessage("Not enough Image control.");
                    }

                    if (null != e.NewSensor)
                    {
                        // Turn on the skeleton stream to receive skeleton frames
                        e.NewSensor.SkeletonStream.Enable();
                        e.NewSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;

                        // Add an event handler to be called whenever there is new color frame data
                        e.NewSensor.SkeletonFrameReady += sK.SensorSkeletonFrameReady;

                        // Start the sensor!
                        try
                        {
                            e.NewSensor.Start();
                        }
                        catch (IOException)
                        {
                            this.sensors.Remove(e.NewSensor);
                            tempSensorSuccessIndicator = false;
                        }
                    }

                    // Create new sensor chooser for new sensors
                    if (this.sensorChoosers.Count <= MaxSensors && tempSensorSuccessIndicator)
                    {
                        // Start hidden so we don't see the "connect sensor" message 
                        this.AddSensorChooser(new Thickness(80, 0, 0, 5), true);
                    }
                }
            }
            else if (null != e.OldSensor)
            {
                // Sensor removed
                KinectSensor result = null;
                result = this.sensors.Find(c => (c.DeviceConnectionId == e.OldSensor.DeviceConnectionId));

                if (null != result && KinectStatus.Connected != e.OldSensor.Status)
                {
                    // Removes the last spare, unused sensor chooser and then sets the sensor chooser for the
                    // camera we just unplugged  hidden so we don't see the "connect sensor" message 
                    this.RemoveSensorChooser(e.OldSensor.DeviceConnectionId, true);

                    this.UnsubscribeAndStopSensor(result);

                    this.sensors.Remove(result);

                    this.dSkeletonKit[e.OldSensor.DeviceConnectionId].DisposeAllStreamWriters();

                    this.dSkeletonKit.Remove(e.OldSensor.DeviceConnectionId);
                    this.qImage.Enqueue(this.dImage[e.OldSensor.DeviceConnectionId]);
                    this.dImage.Remove(e.OldSensor.DeviceConnectionId);

                    if (this.sensors.Count == 0)
                    {
                        this.showStatusMessage("Nothing");
                    }
                    else
                    {
                        this.showStatusMessage(this.sensors.Count + " sensor(s) found");
                    }
              
                    result.Dispose();
                }
            }
        }


        /// <summary>
        /// Remove Sensor Chooser objects
        /// </summary>
        /// <param name="deviceConnectionId">The device ID for the sensor chooser to remove.</param>
        /// <param name="hideUnpluggedSensorChooser">Set true to immediately hide the unplugged sensor chooser.</param>
        private void RemoveSensorChooser(string deviceConnectionId, bool hideUnpluggedSensorChooser)
        {
            // Hide unplugged sensor chooser
            if (hideUnpluggedSensorChooser)
            {
                int resultIdx = this.sensors.FindIndex(0, c => (c.DeviceConnectionId == deviceConnectionId));
                if (0 <= resultIdx && resultIdx < this.sensorchooserStackPanel.Children.Count)
                {
                    this.sensorchooserStackPanel.Children[resultIdx].Visibility = Visibility.Hidden;
                }
            }

            // As long as this is not the only one, we remove the last spare, unused sensor chooser,
            // as we now have another unused one
            if (1 < this.sensorChoosers.Count && 1 < this.sensorchooserStackPanel.Children.Count)
            {
                this.sensorChoosers.RemoveAt(this.sensorChoosers.Count - 1);
                this.sensorchooserStackPanel.Children.RemoveAt(this.sensorchooserStackPanel.Children.Count - 1);
            }

            // Make sure if we have only one that it is forced to be visible so it will ask users to plug in a sensor.
            if (1 == this.sensorChoosers.Count && 1 == this.sensorchooserStackPanel.Children.Count)
            {
                this.sensorchooserStackPanel.Children[0].Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Un-subscribe from events and stop sensor streams
        /// </summary>
        /// <param name="sensor">The sensor object to stop.</param>
        private void UnsubscribeAndStopSensor(KinectSensor sensor)
        {
            if (null == sensor)
            {
                return;
            }

            sensor.Stop();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        private void showStatusMessage(String message)
        {
            this.MessageBox.Text = message;
        }

        # region Event Handler
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartRecordButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (SkeletonKit sK in this.dSkeletonKit.Values)
            {
                sK.SetIsRecord(true);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopRecordButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (SkeletonKit sK in this.dSkeletonKit.Values)
            {
                sK.SetIsRecord(false);
                sK.ResetDistanceRecorder();
            }
        }
        # endregion

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Kinect;

namespace MARK_Alpha.Kit.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    interface InfoRecorderInterface
    {
        void ResetPreviousRecord();

        void SetSkeleton(Skeleton Skel, string ImageIndex);

        void SetJointTyps(JointType[] Jts);

        void SetRecordTime(DateTime Time);

        void SetStreamWriter(StreamWriter appendSW);

        void Execute();

        void WriteVoidLine();
    }
}
